#include <stdio.h>
#include <limits.h>
#include <math.h>

long maxlong(void){
	return LONG_MAX;
}

double upper_bound(long n){
	double upper;
	upper = 1;
	if(n>=6){
		return (double)pow(((double)n/2.0),(double)n);
	}
	else{
		while(n>0){
			upper = upper *  n;
			n--;
		}
		
	return upper;
	}
}

long factorial(long n){
	long fact;
	fact = 1;
	if(n<0){
		return -2;
	}
	else if(upper_bound(n)<maxlong()){
		while(n>0){
			fact *= n;
			n--;
		}
	return fact;	   	   
	}
	else{
		return -1;
	}
}

int main(void) {
    long i;

    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

    
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
    }
    
    return 0;
}

