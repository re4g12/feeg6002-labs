#include<stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include<stdlib.h>
#include<math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used .", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */

double f(double x){
	double fx;
	fx = sqrt(1-x*x);
	return fx;
}

double pi(long n){
	int i;
	double h, s, x, PI, a,b;
	a = -1;
	b = 1;
	h = (b-a)/(double)n;
	s = 0.5 * f(a) + 0.5 * f(b);
	for(i=1; i<=n-1; i++){
		x = a + i*h;
		s+= f(x);
	}
	PI = s * h * 2;
	return PI;
}

int main(void) {
    /* Declarations */



    /* Code */
    START;               /* Timing measurement starts here */
    /* Code to be written by student, calling functions from here is fine
       if desired
    */
	printf("%f",pi(5));


    STOP;                /* Timing measurement stops here */
    PRINTTIME;           /* Print timing results */
    return 0;
}

