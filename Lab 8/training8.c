#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* mix(char *s1, char *s2){
	char *r;
	int a, b = 0; 
	long len;
	len = strlen(s1);
	r=(char *)malloc(sizeof(s1));
	if(r == NULL){
		printf("Memory allocation faild.\n");
		return NULL;
	}
	else{
		for(a = 0, b = 0; b<=len; a+=2, b++){
			r[a] = s1[b];
			r[a+1] = s2[b];
		}
		return r;
	}
}	 	 
		

void use_mix(void) {
    char s1[] = "Hello World";
    char s2[] = "1234567890!";

    printf("s1 = %s\n", s1);
    printf("s2 = %s\n", s2);
    printf("r  = %s\n", mix(s1, s2));
}

