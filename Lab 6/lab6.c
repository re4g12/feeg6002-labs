#include <stdio.h>
#include <string.h>


void lstrip(char s[]) {
	long len;
	int i, n, k;
	len = strlen(s);
	i = 0;
	n=0;
	k = 0;
	while(s[i] == ' '){
		i++;
		n++;
	}
	for(k=i; k<=len; k++){
		s[k-i] = s[n];
		n++;
	}
	s[len-i+1] = '\0';
	    
}


int main(void) {
  char test1[] = "   Hello World";

  printf("Original string reads  : |%s|\n", test1);
  lstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}

