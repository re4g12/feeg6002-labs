#include <stdio.h>
#include <string.h>


void rstrip(char s[]) {
	long len;
	int i;
	len = strlen(s)-1;
	i = 0;
	while(s[len] == ' '){
		i++;
		len--;
	}
	s[len+1] = '\0';
	    
}


int main(void) {
  char test1[] = "Hello World   ";

  printf("Original string reads  : |%s|\n", test1);
  rstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}

