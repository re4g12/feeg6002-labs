"""Python script developed by Richard Earl - 25312898 for the completion of the
FEEG6002 Advanced Computational Methods module at the University of Southampton
Completion date 04/01/2016""" 

import numpy as np
import scipy as sp
import math
import scipy.linalg
import matplotlib
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

"""Comments:
    -n is a global variable to change the number of unknown mesh points of
    problem
    -n must be odd and greater than 3 so that there is a point at the centre
    x,y =  0.5 to perform the required checks
    -a plot function has been included to generate graphs but the code to call
    plot function has been commented out. This provides a visual check for the
    code but has been commented out to ensure the script executed the code as
    of the design brief (a single line out put for each question.
    - Each of the four questions has been defined and the solver calls on the
    other functions that have been created.
    -the four questions return an n x n matrix of rho values, but only the
    central value at x,y = 0.5 is printed to the screen as a check.
    -Sources used have been referenced in the write up of this report"""

n = 19

def question1():
    """Calls all relevant questions required to obtain the answers for question 1"""
    matrix_full = n+2
    h = 1.0/matrix_full
    a = A()
    b = B(h)
    u = np.linalg.solve(a,b)
    U=embed_unknowns(u)
    #Uplots(U,matrix_full, 'Basic 4 Point', n)
    rho = check_output(U,h,matrix_full)
    return rho

def A():
    """creates a 2D matrix of size n x n of n^2 simultaneous equations for the
    the 4 point stencil solver used in questions 1 and 2. this is in the form
    Ax = b. Code developed from the notes proveded by Simon Cox and Samuel
    Sinayako and modified for this particular PDE"""
    a = np.zeros([n**2, n**2])
    #build interior
    for i in range(1,n-1):
        for j in range(1,n-1):
            north = (i-1)*n+j
            west = i*n+j-1
            index= i*n+j
            east = i*n+j+1
            south = (i+1)*n+j
    
            a[index,north]=1
            a[index,west] =1
            a[index,index]=-4
            a[index,east] =1
            a[index,south]=1

    #North (nothing further North)
    i=0 #First Row Number
    for j in range(1,n-1):
        #north = (i-1)*n+j
        west = i*n+j-1
        index= i*n+j
        east = i*n+j+1
        south = (i+1)*n+j

        #a[index,north]=1
        a[index,west] =1
        a[index,index]=-4
        a[index,east] =1
        a[index,south]=1
        
    #East(nothing further East)
    j=n-1 # Last Column number
    for i in range(1,n-1):
        north = (i-1)*n+j
        west = i*n+j-1
        index= i*n+j
        #east = i*n+j+1
        south = (i+1)*n+j

        a[index,north]=1
        a[index,west] =1
        a[index,index]=-4
        #a[index,east] =1
        a[index,south]=1
        
        
    #South (nothing further South)
    i=n-1 # Last row number
    for j in range(1,n-1):
        north = (i-1)*n+j
        west = i*n+j-1
        index= i*n+j
        east = i*n+j+1
        #south = (i+1)*n+j

        a[index,north]=1
        a[index,west] =1
        a[index,index]=-4
        a[index,east] =1
        #a[index,south]=1
    #West (nothing further West)
    j=0 #First Column Number
    for i in range(1,n-1):
        north = (i-1)*n+j
        #west = i*n+j-1
        index= i*n+j
        east = i*n+j+1
        south = (i+1)*n+j

        a[index,north]=1
        #a[index,west] =1
        a[index,index]=-4
        a[index,east] =1
        a[index,south]=1

    #corners
    #Top Left
    i=0
    j=0
    index= i*n+j
    east = i*n+j+1
    south = (i+1)*n+j

    a[index,index]=-4
    a[index,east] =1
    a[index,south]=1

    #Top Right
    i=0
    j=n-1
    west = i*n+j-1
    index= i*n+j
    south = (i+1)*n+j

    a[index,west] =1
    a[index,index]=-4
    a[index,south]=1

    #Bottom Left
    i=n-1
    j=0
    north = (i-1)*n+j
    index= i*n+j
    east = i*n+j+1

    a[index,north]=1
    a[index,index]=-4
    a[index,east] =1

    #Bottom Right
    i=n-1
    j=n-1
    north = (i-1)*n+j
    west = i*n+j-1
    index= i*n+j

    a[index,north]=1
    a[index,west] =1
    a[index,index]=-4

    return a

def B(h):
    """returns the vector b to solve the matrix equation Ax = b for a 4 point
    stencil used in questions 1 and 2"""
    b = np.zeros([n**2,1])
    b[(n**2 -1) /2] = 2*h**2
    return b

def embed_unknowns(solution):
    """Takes a column of n^2 solutions and reshapes them into a 2D grid, before
    embedding the grin into the boundary conditions given by the design brief.
    This is used in questions 1 and 2.Code developed from the notes proveded by
    Simon Cox and Samuel Sinayako and modified for this particular PDE"""
    solution_wrap = np.reshape(solution, [n,n])
    size = solution_wrap.shape[0]
    full_solution = np.zeros([size+2, size + 2])
    for i in range(1, size+1):
        for j in range(1, size+1):
            full_solution[i,j]=solution_wrap[i-1, j-1]
    return full_solution

def Uplots(U,matrix_full, solver, n):
    """plots the solution on a 3D graph for a visual representation. used for all
    four questions to produce figures for the report"""
    fig = plt.figure()
    Uplot = fig.gca(projection = '3d')
    h2 = 1.0/(matrix_full-1)
    X = np.arange(0, matrix_full, 1)*h2
    Y = np.arange(0, matrix_full, 1)*h2
    X,Y = np.meshgrid(X,Y)
    surf = Uplot.plot_wireframe(X, Y, U, rstride=1, cstride=1)
    Uplot.set_xlabel('x position')
    Uplot.set_ylabel('y position')
    Uplot.set_zlabel('U(x,y)')
    fig.suptitle('A plot showing U(x,y) by using the '+solver+' solver with n = '+str(n)+' unknown mesh points')
    plt.show()

def check_output(U,h,matrix_full):
    """Calculated the value of the laplacian at all points using the 4 point stencil.
    this checks that the solution does infact answer the question given, that it should
    equal 2 at x,y = 0.5 and zero everywhere else"""
    rho = np.zeros([n,n])
    for i in range(1,matrix_full-1):
        for j in range(1,matrix_full-1):
            Check = (U[i,j-1]+U[i-1,j]-4*U[i,j]+U[i+1,j]+U[i,j+1])/h**2
            rho[i-1,j-1] = Check
    return rho

def question2():
    """Calls all relevant questions required to obtain the answers for question 2"""
    matrix_full = n+2
    h = 1.0/matrix_full
    a = A()
    b = B(h)
    u = np.zeros(n**2)
    u = Gauss_Seidel_Relaxation(next_itteration, u, a, b)
    U = embed_unknowns(u)
    #Uplots(U,matrix_full, 'Gauss-Seidel relaxation', n)
    rho = check_output(U, h, matrix_full)
    return rho

def next_itteration(x, omega, A, b):
    """Called by the Gauss_Seidel_relaxation function to calculate the next iteration"""
    n = len(x)
    for i in range(n):
        s = 0
        for j in range(n):
            if j != i:
                s += A[i,j]*x[j]
        x[i] = omega/A[i,i]*(b[i]-s)+(1-omega)*x[i]
    return x
    
def Gauss_Seidel_Relaxation(next_itteration, x, A, b, tol=1.0e-9):
    """This code performs the Gauss-seidel itteretive method to converge on a solution
    of Ax = b. this is used in questions 2 and 4."""
    omega = 1
    k = 10
    p = 1
    for i in range(1, 501):
        xold = x.copy()
        x = next_itteration(x, omega, A, b)
        delta_x = math.sqrt(np.dot(x-xold, x-xold))
        if delta_x < tol:
            return x
            return i
            return omega
        elif i == k:
            delta_x1 = delta_x
        elif i == k+p:
            delta_x2 = delta_x
            omega = 2.0/(1.0+math.sqrt(1.0-(delta_x2/delta_x1)**(1.0/p)))
    print'Gauss-Seidel failed to converge'
    return 0

def question3():
    """Calls all relevant questions required to obtain the answers for question 4"""
    matrix_full = n+4
    h = 1.0/matrix_full
    a = A_8point_stencil()
    b = B_8point_stencil(h)
    u = np.linalg.solve(a,b)
    U = embed_unknowns_8point_stencil(u)
    #Uplots(U, matrix_full, '8 point stencil', n)
    rho = check_output_8point_stencil(U, h, matrix_full)
    return rho

def A_8point_stencil():
    """creates a 2D grid of n^2 simulatanious equations for an 8 point stencil solver
    in the form Ax = b. this A matrix is used in questoin 3. Code developed from
    the notes proveded by Simon Cox and Samuel Sinayako and modified for this
    particular PDE. This is essentially the same code as in question 1 but with
    added sections to account for the 8 point stencil."""
    a=np.zeros([n**2,n**2])
    for i in range(2,n-2):
        for j in range(2, n-2):
            North1 = (i-1)*n+j
            North2 = (i-2)*n+j
            East1 = i*n+j+1
            East2 = i*n+j+2
            South1 = (i+1)*n+j
            South2 = (i+2)*n+j
            West1 = i*n+j-1
            West2 = i*n+j-2
            Index= i*n+j

            a[Index,North1]=16
            a[Index,North2]=-1
            a[Index,East1] =16
            a[Index,East2]=-1
            a[Index,South1]=16
            a[Index,South2]=-1
            a[Index,West1] =16
            a[Index,West2]=-1
            a[Index,Index]=-60

    #inner edges:

    #North inner - North2
    i = 1
    for j in range(2,n-2):
        North1 = (i-1)*n+j
        #North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        #a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #East Inner - East 2
    j = n-2
    for i in range(2,n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        East1 = i*n+j+1
        #East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        a[Index,East1] =16
        #a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #South inner - South 2
    i = n-2
    for j in range(2, n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        South1 = (i+1)*n+j
        #South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        a[Index,South1]=16
        #a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #West inner - West 2
    j=1
    for i in range(2, n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        West1 = i*n+j-1
        #West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        a[Index,West1] =16
        #a[Index,West2]=-1
        a[Index,Index]=-60


    #Outer Edges

    # North outer edge - North 1 and North 2
    i = 0
    for j in range(2,n-2):
        #North1 = (i-1)*n+j
        #North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        #a[Index,North1]=16
        #a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #East outer edge - East 1 and East 2
    j = n-1
    for i in range(2, n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        #East1 = i*n+j+1
        #East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        #a[Index,East1] =16
        #a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #South outer edge - South 1 and South 2
    i = n-1
    for j in range(2, n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        #South1 = (i+1)*n+j
        #South2 = (i+2)*n+j
        West1 = i*n+j-1
        West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        #a[Index,South1]=16
        #a[Index,South2]=-1
        a[Index,West1] =16
        a[Index,West2]=-1
        a[Index,Index]=-60

    #West outer edge - West 1 and West 2
    j = 0
    for i in range(2,n-2):
        North1 = (i-1)*n+j
        North2 = (i-2)*n+j
        East1 = i*n+j+1
        East2 = i*n+j+2
        South1 = (i+1)*n+j
        South2 = (i+2)*n+j
        #West1 = i*n+j-1
        #West2 = i*n+j-2
        Index= i*n+j

        a[Index,North1]=16
        a[Index,North2]=-1
        a[Index,East1] =16
        a[Index,East2]=-1
        a[Index,South1]=16
        a[Index,South2]=-1
        #a[Index,West1] =16
        #a[Index,West2]=-1
        a[Index,Index]=-60

        #Corner edges

    #North2west - North 1, North 2 and West 2
    i = 0
    j = 1
    #North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    #a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #North2east - North1, North2 and East2
    i = 0
    j = n-2
    #North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    #a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #East2north - East1, East2 and North2
    i = 1
    j = n-1
    North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    #East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    #a[Index,North2]=-1
    #a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #East2south - East1, East2 and South 2
    i = n-2
    j = n-1
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    #East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    #a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #South2east - South1, South2 and East2
    i = n-1
    j = n-2
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    #East2 = i*n+j+2
    #South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    #a[Index,East2]=-1
    #a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #South2west - South1, South2 and West2
    i = n-1
    j = 1
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    #South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    #a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #West2south - West1, West2 and South2
    i = n-2
    j = 0
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    #West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    #a[Index,South2]=-1
    #a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #West2North - West1, West2 and North2
    i = 1
    j = 0
    North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    #West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    #a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #inner corners:

    #Northeast inner - North2 and East2
    i=1
    j=n-2
    North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #Southeast inner - South 2 and East 2
    i = n-2
    j = n-2
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60
    
    #Southwest inner - South 2 and West 2
    i = n-2
    j = 1
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #Northwest inner - North 2 and West 2
    i = 1
    j = 1
    North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    #Far Corners:

    #Far Northeast - North1, North2, East1 and East2
    i = 0
    j = n-1
    #North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    #East1 = i*n+j+1
    #East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    #a[Index,North1]=16
    #a[Index,North2]=-1
    #a[Index,East1] =16
    #a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #Far Southeast - South1, South2, East1 and East2
    i = n-1
    j = n-1
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    #East1 = i*n+j+1
    #East2 = i*n+j+2
    #South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    West1 = i*n+j-1
    West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    #a[Index,East1] =16
    #a[Index,East2]=-1
    #a[Index,South1]=16
    #a[Index,South2]=-1
    a[Index,West1] =16
    a[Index,West2]=-1
    a[Index,Index]=-60

    #Far Southwest - South1, South2, West1 and West2
    i = n-1
    j = 0
    North1 = (i-1)*n+j
    North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    #South1 = (i+1)*n+j
    #South2 = (i+2)*n+j
    #West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    a[Index,North1]=16
    a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    #a[Index,South1]=16
    #a[Index,South2]=-1
    #a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60
    
    #Far Northwest - North1, North2, West1 and West2
    i = 0
    j = 0
    #North1 = (i-1)*n+j
    #North2 = (i-2)*n+j
    East1 = i*n+j+1
    East2 = i*n+j+2
    South1 = (i+1)*n+j
    South2 = (i+2)*n+j
    #West1 = i*n+j-1
    #West2 = i*n+j-2
    Index= i*n+j

    #a[Index,North1]=16
    #a[Index,North2]=-1
    a[Index,East1] =16
    a[Index,East2]=-1
    a[Index,South1]=16
    a[Index,South2]=-1
    #a[Index,West1] =16
    #a[Index,West2]=-1
    a[Index,Index]=-60

    return a

def B_8point_stencil(h):
    """returns the b vector for the equation Ax=b using an 8 point stencil required
    for question 3"""
    b = np.zeros([n**2,1])
    b[(n**2 -1) /2] = 24*h**2
    return b

def embed_unknowns_8point_stencil(solution):
    """Executes in the same way as the embed_unknown used previously, only this embeds
    the grid for an 8 point stencil used in question 3.Code developed from the
    notes proveded by Simon Cox and Samuel Sinayako and modified for this particular PDE"""
    solution_wrap=np.reshape(solution,[n,n])
    size=solution_wrap.shape[0]
    full_solution=np.zeros([size+4,size+4])
    for i in range(2,size+2):
        for j in range(2,size+2):
            full_solution[i,j]=solution_wrap[i-2,j-2]
    return full_solution
    
def check_output_8point_stencil(U,h,matrix_full):
    """checks that the solution obtained by the 8 point stencil solver returns the correct
    solution at the centre point, x,y = 0.5 = 2 and zero everywhere else"""
    rho = np.zeros([n,n])
    for i in range(2,matrix_full-2):
        for j in range(2,matrix_full-2):
            check = (-U[i,j-2]+16*U[i,j-1]-U[i-2,j]+16*U[i-1,j]-60*U[i,j]+16*U[i+1,j]-U[i+2,j]+16*U[i,j+1]-U[i,j+2])/(12.*h**2)
            rho[i-2,j-2] = check
    return rho

def question4():
    """Calls all relevant questions required to obtain the answers for question 4"""
    matrix_full = n+2
    h = 1.0/matrix_full
    a = A_red_black()
    b = B_red_black(h)
    u = np.zeros(n**2)
    u = Gauss_Seidel_Relaxation(next_itteration, u, a, b)
    U = embed_unknowns_red_black(u)
    #Uplots(U,matrix_full, 'red-black Gauss-Seidel relaxation', n)
    rho = check_output(U, h, matrix_full)
    return rho
    

def red_black_checker(i,j):
    """determins whether the node within the mesh at a point (i,j) should be red
    or black. This is done by checking whether the point is odd or even (devisible
    by 2). It returns the index of the point an the factor used to properly create a
    red-black grid format"""
    nodetotal = i*n+j
    if (nodetotal%2) == 0:
        f = n**2
        nodetotal = nodetotal/2
    else:
        f =0
        nodetotal = (n**2 + nodetotal)/2
    return (f,nodetotal)

def A_red_black():
    """creates the A matrix of n^2 simulataneous equations using a four point stencil
    with red and black mapping, used in the form Ax = b required for question 4"""
    a=np.zeros([n**2,n**2])

    #build interior
    for i in range(1,n-1):
        for j in range(1, n-1):
            (f, nodetotal)=red_black_checker(i,j)
            North = ((i-1)*n +j +f)/2
            East = (i*n + j +1 +f)/2
            South = ((i+1)*n +j +f)/2
            West = (i*n +j -1 +f)/2
            a[nodetotal, North] =1
            a[nodetotal, East] =1
            a[nodetotal, South] =1
            a[nodetotal, West] =1
            a[nodetotal, nodetotal] = -4

    #edges

    #North
    i=0
    for j in range(1,n-1):
        (f, nodetotal) = red_black_checker(i,j)
        #North = ((i-1)*n +j +f)/2
        East = (i*n + j +1 +f)/2
        South = ((i+1)*n +j +f)/2
        West = (i*n +j -1 +f)/2
        #a[nodetotal, North] =1
        a[nodetotal, East] =1
        a[nodetotal, South] =1
        a[nodetotal, West] =1
        a[nodetotal, nodetotal] = -4

    #East
    j = n-1
    for i in range(1, n-1):
        (f, nodetotal) = red_black_checker(i,j)
        North = ((i-1)*n +j +f)/2
        #East = (i*n + j +1 +f)/2
        South = ((i+1)*n +j +f)/2
        West = (i*n +j -1 +f)/2
        a[nodetotal, North] =1
        #a[nodetotal, East] =1
        a[nodetotal, South] =1
        a[nodetotal, West] =1
        a[nodetotal, nodetotal] = -4

    #South
    i = n-1
    for j in range(1, n-1):
        (f, nodetotal) = red_black_checker(i,j)
        North = ((i-1)*n +j +f)/2
        East = (i*n + j +1 +f)/2
        #South = ((i+1)*n +j +f)/2
        West = (i*n +j -1 +f)/2
        a[nodetotal, North] =1
        a[nodetotal, East] =1
        #a[nodetotal, South] =1
        a[nodetotal, West] =1
        a[nodetotal, nodetotal] = -4

    #West
    j = 0
    for i in range(1, n-1):
        (f, nodetotal) = red_black_checker(i,j)
        North = ((i-1)*n +j +f)/2
        East = (i*n + j +1 +f)/2
        South = ((i+1)*n +j +f)/2
        #West = (i*n +j -1 +f)/2
        a[nodetotal, North] =1
        a[nodetotal, East] =1
        a[nodetotal, South] =1
        #a[nodetotal, West] =1
        a[nodetotal, nodetotal] = -4

    #Corners

    #Northeast
    i = 0
    j = n-1
    (f, nodetotal) = red_black_checker(i,j)
    #North = ((i-1)*n +j +f)/2
    #East = (i*n + j +1 +f)/2
    South = ((i+1)*n +j +f)/2
    West = (i*n +j -1 +f)/2
    #a[nodetotal, North] =1
    #a[nodetotal, East] =1
    a[nodetotal, South] =1
    a[nodetotal, West] =1
    a[nodetotal, nodetotal] = -4

    #Southeast
    i = n-1
    j = n-1
    (f, nodetotal) = red_black_checker(i,j)
    North = ((i-1)*n +j +f)/2
    #East = (i*n + j +1 +f)/2
    #South = ((i+1)*n +j +f)/2
    West = (i*n +j -1 +f)/2
    a[nodetotal, North] =1
    #a[nodetotal, East] =1
    #a[nodetotal, South] =1
    a[nodetotal, West] =1
    a[nodetotal, nodetotal] = -4

    #Southwest
    i = n-1
    j = 0
    (f, nodetotal) = red_black_checker(i,j)
    North = ((i-1)*n +j +f)/2
    East = (i*n + j +1 +f)/2
    #South = ((i+1)*n +j +f)/2
    #West = (i*n +j -1 +f)/2
    a[nodetotal, North] =1
    a[nodetotal, East] =1
    #a[nodetotal, South] =1
    #a[nodetotal, West] =1
    a[nodetotal, nodetotal] = -4

    #Northwest
    i = 0
    j = 0
    (f, nodetotal) = red_black_checker(i,j)
    #North = ((i-1)*n +j +f)/2
    East = (i*n + j +1 +f)/2
    South = ((i+1)*n +j +f)/2
    #West = (i*n +j -1 +f)/2
    #a[nodetotal, North] =1
    a[nodetotal, East] =1
    a[nodetotal, South] =1
    #a[nodetotal, West] =1
    a[nodetotal, nodetotal] = -4

    return a

def B_red_black(h):
    """returns the vector b required to solve the equation Ax = b used by the red
    black solver in question 4"""
    b = np.zeros([n**2,1])
    b[(n**2 -1) /4] = 2*h**2
    return b

def embed_unknowns_red_black(solution):
    """runs the same principle as the embed_unknowns function used in question on
    but for the red and black mapping. used in question 4"""
    solution_reverse = np.zeros(n**2)
    for x in range(n**2):
        x2 = n**2/2.0
        if x<x2:
            solution_reverse[2*x] = solution[x]
        else:
            solution_reverse[2*x-n**2] = solution[x]
    solution_wrap=np.reshape(solution_reverse,[n,n])
    size=solution_wrap.shape[0]
    full_solution=np.zeros([size+2,size+2])
    for i in range(1,size+1):
        for j in range(1,size+1):
            full_solution[i,j]=solution_wrap[i-1,j-1]
    return full_solution

rho1 = question1()
rho2 = question2()
rho3 = question3()
rho4 = question4()
print 'solver 1 - 4 point stencil:'
print u'\u03C1(0.5,0.5) = '+str(rho1[n/2,n/2])
print 'solver 2 - Gauss-Seidel relaxation:'
print u'\u03C1(0.5,0.5) = '+str(rho2[n/2,n/2])
print 'solver 3 - eight point stencil:'
print u'\u03C1(0.5,0.5) = '+str(rho3[n/2,n/2])
print 'solver 4 - red and black Gauss-Seidel relaxation:'
print u'\u03C1(0.5,0.5) = '+str(rho4[n/2,n/2])
