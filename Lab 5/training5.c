#include <stdio.h>

long string_length(char s[]){
	long total;
	int i;
	i =0;
	total = 0;
	while( s[i] != '\0'){
		total ++;
		i++;
	}
	return total;
}
		

int main(void) {
  char s1[]="Hello";
  char s2[]="x";
  char s3[]="line 1\tline 2\n";

  printf("%20s | %s\n", "string_length(s)", "s");
  printf("%20ld | %s\n", string_length(s1), s1);
  printf("%20ld | %s\n", string_length(s2), s2);
  printf("%20ld | %s\n", string_length(s3), s3);
  return 0;
}

