#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

long string_length(char s[]){
	long total;
	int i;
	i =0;
	total = 0;
	while( s[i] != '\0'){
		total ++;
		i++;
	}
	return total;
}
		

void reverse(char source[], char target[]){
	int i;
	long len;
	i = 0;
	len = string_length(source);
	while(source[i] != '\0'){
		target[i] = source[len - i-1];
		i++;
	}
	target[len] = '\0';
} 

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

