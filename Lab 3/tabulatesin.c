#include <stdio.h>
#include <math.h>
#define N 10
#define XMAX 10
#define XMIN 1

int main(void){
	double x, fx;
	int n = 0;
	for(x = XMIN; x <= XMAX; x = XMIN + n*(XMAX - XMIN)/(N - 1.0)){
		fx=sin(x);
		printf("%f %f\n", x, fx);
		n++;
	}
	return 0;
}

