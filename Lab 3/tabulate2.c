#include <stdio.h>
#include <math.h>
#define N 10
#define XMAX 10
#define XMIN 1

int main(void){
	double x, fxs, fxc;
	int n = 0;
	for(x = XMIN; x <= XMAX; x = XMIN + n*(XMAX - XMIN)/(N - 1.0)){
		fxs=sin(x);
		fxc=cos(x);
		printf("%f %f %f\n", x, fxs, fxc);
		n++;
	}
	return 0;
}

