#include <stdio.h>

int main(void){
	double s, debt, rate, interest, total, frac;
	int month;
	total = 0.0;
	s=1000;
	debt=1000;
	rate = 0.03;
	for(month = 1; month <= 24; month++){
		interest = debt *rate;
		total += interest;
		debt += interest;
		frac = total/s *100;
		printf("month %d: debt=%7.2f, interest = %5.2f, total_interest=%7.2f, frac = %6.2f%%\n", month, debt, interest, total, frac);
	}
	return 0;
}

